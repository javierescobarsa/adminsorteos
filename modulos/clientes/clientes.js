


var contentClientes = "";
var listClientes = [];



function filtrarNombreDet(nombre) {
  nombre = nombre.toLowerCase();
  filtroNombre = "";
  contadorClientes = 0;
  nombreCorrectos = listClientes.filter(function (el) {
    nomActual = el.nombre.toLowerCase();
    return nomActual.startsWith(nombre);
  })

  nombreCorrectos.forEach(function (docCup) {
    contadorClientes++;
    filtroNombre += `
            <tr style='font-size:70%;'>
            <td style="width:25%;"> `+ docCup.nombre + ` `+ docCup.apellido + `</td>
<td style="width:25%;"> `+ docCup.rut + `</td>
<td style="width:10%;"> `+ docCup.clasificacion + `</td>
<td style="width:10%;"> `+ docCup.sucursal + `</td>
<td style="width:10%;"> `+ docCup.fecha + `</td>
<td style="width:10%;"> `+ docCup.hora + `</td>
<td style="width:10%;"><i class="fas fa-bars" onclick="cargarModificarCliDet('`+ docCup.id + `','` + docCup.nombre + `','` + docCup.apellido + `','` + docCup.fono + `','` + docCup.rut + `','` + docCup.clasificacion + `','` + docCup.sucursal + `','` + docCup.monto + `',` + docCup.contador + `,'` + docCup.fecha + `','` + docCup.hora + `')"></i> </td>
            </tr>
`
  });
  $("#contadorClientes").html(contadorClientes)
  $('#cuerpoCliente').html(filtroNombre);
}

function filtrarRutDet(rut) {
  filtroRut = "";
  contadorClientes = 0;
  rutCorrectos = listClientes.filter(function (el) {
    return el.rut.startsWith(rut);
  })
  rutCorrectos.forEach(function (docCup) {
    contadorClientes++;
    filtroRut += `
            <tr style='font-size:70%;'>
            <td style="width:25%;"> `+ docCup.nombre + ` `+ docCup.apellido + `</td>
<td style="width:25%;"> `+ docCup.rut + `</td>
<td style="width:10%;"> `+ docCup.clasificacion + `</td>
<td style="width:10%;"> `+ docCup.sucursal + `</td>
<td style="width:10%;"> `+ docCup.fecha + `</td>
<td style="width:10%;"> `+ docCup.hora + `</td>
<td style="width:10%;"><i class="fas fa-bars" onclick="cargarModificarCliDet('`+ docCup.id + `','` + docCup.nombre + `','` + docCup.apellido + `','` + docCup.fono + `','` + docCup.rut + `','` + docCup.clasificacion + `','` + docCup.sucursal + `','` + docCup.monto + `',` + docCup.contador + `,'` + docCup.fecha + `','` + docCup.hora + `')"></i> </td>
            </tr>
`
  });
  $("#contadorClientes").html(contadorClientes)
  $('#cuerpoCliente').html(filtroRut);
}


function filtrarNombre(nombre) {
  nombre = nombre.toLowerCase();
  filtroNombre = "";
  contadorClientes = 0;
  nombreCorrectos = listClientes.filter(function (el) {
    nomActual = el.nombre.toLowerCase();
    return nomActual.startsWith(nombre);
  })

  nombreCorrectos.forEach(function (docCup) {
    clase=''
    hora='';
    if(docCup.fecha==obtenerFecha()){
        clase='table-success'
        hora=docCup.hora
    }
    contadorClientes++;
    filtroNombre += `
            <tr style='font-size:70%;' class="`+clase+`">
            <td style="width:30%;"> `+ docCup.nombre + ` ` + docCup.apellido + `</td>
<td style="width:30%;"> `+ docCup.rut + `</td>
<td style="width:15%;"> `+ docCup.clasificacion + `</td>
<td style="width:15%;"> `+ docCup.sucursal + ` <p>`+ hora + ` </p></td>
<td style="width:10%;"><i class="fas fa-bars" onclick="cargarModificarCli('`+ docCup.id + `','` + docCup.nombre + `','` + docCup.apellido + `','` + docCup.fono + `','` + docCup.rut + `','` + docCup.clasificacion + `','` + docCup.sucursal + `','` + docCup.monto + `',` + docCup.contador + `)"></i> </td>
            </tr>
`
  });
  $("#contadorClientes").html(contadorClientes)
  $('#cuerpoCliente').html(filtroNombre);
}

function filtrarSucursal(sucursal) {

  filtroSucursal = "";
  contadorClientes = 0;
  rutCorrectos = listClientes.filter(function (el) {
    return el.sucursal.startsWith(sucursal);
  })
  rutCorrectos.forEach(function (docCup) {
    clase=''
    hora='';
    if(docCup.fecha==obtenerFecha()){
        clase='table-success'
        hora=docCup.hora
    }
    console.log(docCup.fecha)
    console.log(clase)
    contadorClientes++;
    filtroSucursal += `
    <tr style='font-size:70%;' class="`+clase+`">
<td style="width:30%;"> `+ docCup.nombre + ` ` + docCup.apellido + `</td>
<td style="width:30%;"> `+ docCup.rut + `</td>
<td style="width:15%;"> `+ docCup.clasificacion + `</td>
<td style="width:15%;"> `+ docCup.sucursal + ` <p>`+ hora + ` </p></td>
<td style="width:10%;"><i class="fas fa-bars" onclick="cargarModificarCli('`+ docCup.id + `','` + docCup.nombre + `','` + docCup.apellido + `','` + docCup.fono + `','` + docCup.rut + `','` + docCup.clasificacion + `','` + docCup.sucursal + `','` + docCup.monto + `',` + docCup.contador + `)"></i> </td>
            </tr>
`
  });
  $("#contadorClientes").html(contadorClientes)
  $('#cuerpoCliente').html(filtroSucursal);
}


function eliminarCliente() {
  var r = confirm("Esta seguro que desea eliminar al cliente?");
  if (r == true) {
    db.collection("clientes").doc(localStorage.clienteSeleccionado).delete().then(function () {
      console.log("Cliente Eliminado");
      cargadorModulo("app", "clientes", "clientes");
    }).catch(function (error) {
    });
  }

}

function cargarModificarCli(cli, nombre, apellido, fono, rut, clasificacion, sucursal, monto, contador) {
  console.log(apellido)
  localStorage.clienteSeleccionado = cli;
  localStorage.nombre = nombre
  localStorage.apellido = apellido
  localStorage.fono = fono;
  localStorage.rut = rut
  localStorage.clasificacion = clasificacion
  localStorage.sucursal = sucursal
  localStorage.monto = monto
  localStorage.contador = contador
  cargadorModulo("app", "clientes", "modificar");
}


function cargarModificarCliDet(cli, nombre, apellido, fono, rut, clasificacion, sucursal, monto, contador, fecha, hora) {
  localStorage.clienteSeleccionado = cli;
  localStorage.nombre = nombre
  localStorage.apellido = apellido
  localStorage.fono = fono;
  localStorage.rut = rut
  localStorage.clasificacion = clasificacion
  localStorage.sucursal = sucursal
  localStorage.monto = monto
  localStorage.contador = contador
  localStorage.fecha = fecha
  localStorage.hora = hora
  cargadorModulo("app", "clientes", "modificarDetalle");
}
function guardarModificar(nombre, apellido, fono, contador, clasificacion, local) {
  var washingtonRef = db.collection("clientes").doc(localStorage.clienteSeleccionado);
  return washingtonRef.update({
    nombre: nombre,
    apellido: apellido,
    clasificacion: clasificacion,
    contador: contador,
    sucursal: local,
    fono: fono
  })


}

function guardarModificarDet(nombre, apellido, fono, contador, clasificacion, local, fecha, hora) {
  var washingtonRef = db.collection("clientes").doc(localStorage.clienteSeleccionado);
  return washingtonRef.update({
    nombre: nombre,
    apellido: apellido,
    clasificacion: clasificacion,
    contador: contador,
    sucursal: local,
    fono: fono,
    fecha: fecha
  })
}


function modificarClienteDet(nombre, apellido, fono, contador, clasificacion, local, fecha, hora) {

  if (listClientes.filter(function (el) {

    return el.rut == rut
  }).length < 1) {
    guardarModificarDet(nombre, apellido, fono, contador, clasificacion, local, fecha, hora)

    setTimeout(() => {
      cargadorModulo("app", "clientes", "clientes");
    }, 600);
  } else {
    alert("Ya existe un cliente registrado con ese rut")
  }
}
function modificarCliente(nombre, apellido, fono, contador, clasificacion, local) {

  if (listClientes.filter(function (el) {

    return el.rut == rut
  }).length < 1) {
    guardarModificar(nombre, apellido, fono, contador, clasificacion, local)

    setTimeout(() => {
      cargadorModulo("app", "clientes", "clientes");
    }, 600);
  } else {
    alert("Ya existe un cliente registrado con ese rut")
  }
}


function registrarCliente(nombre, apellido, rut, dv, clasificacion, local, fono) {
  console.log("apellido Actual")
  dv = dv.toLowerCase()
  rut += "-" + dv
  if (listClientes.filter(function (el) {

    return el.rut == rut
  }).length < 1) {

    db.collection("clientes").add({
      rut: rut,
      nombre: nombre,
      apellido: apellido,
      clasificacion: clasificacion,
      monto: 0,
      contador: 0,
      fecha: "01/01/1990",
      hora: "10:00",
      sucursal: local,
      fono: fono
    })

    setTimeout(() => {
      cargadorModulo("app", "clientes", "clientes");
      console.log("actualizando clientes")
    }, 1000);

  } else {
    alert("Ya existe un cliente registrado con ese rut")
  }
}