$( document ).ready(function() {
    cargadorModulo("navBar", "navBar", "selecionado");

});

var contentSucursales="";

    $(document).ready(function () {

        db.collection("sucursales")
        .onSnapshot(function (queryCup) {
             listSucursales = [];
             contentSucursales="";
            contadorCupones=0;
            queryCup.forEach(function (docCup) {
    
                listSucursales.push({
                    id: docCup.id,
                    nombre: docCup.data().nombre,
                    monto: docCup.data().monto
                }
                );
                contentSucursales+=`
                <div class="col-6 ">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title" onclick="cargarSucursal('`+docCup.data().nombre+`','`+docCup.id+`')">`+docCup.data().nombre+`</h5>
                        <h6 class="card-subtitle mb-2 text-muted" id="montoAcumuladoOsorno">Cupones hoy: </h6>
                        <p class="success-text">$`+puntuar(docCup.data().monto)+`</p>
                    </div>
                </div>
            </div>`
            });
            $('#cuerpoSucursales').html(contentSucursales);
            console.log(contentSucursales)
         
        });
        
      });

function cargarSucursal(sucursal){
    localStorage.sucursal=sucursal;
    cargadorModulo("navBar", "navBar", "selecionado");
    cargadorModulo("app", "clientes", "clientes");


}